/*

[ToC]

1.  AUTO TITLING
2.  AUTO ACTIVE INDICATOR SIDEBAR
3.	POP OVER INIT

*/
$(document).ready(function(){
	//////////////////////////////////////////// 1. Auto Titling ///////////////////////////////////////////
	var getTitle = $('.d-ib').html();
	var title;

	typeof getTitle == 'undefined' ? title = 'IoT - MPG' : title = getTitle + ' - IoT - MPG';
	$('html head').find('title').text(title);
	
	//////////////////////////////////// 2. Auto Active Indicator Sidebar ///////////////////////////////////
	$(".sidenav-label").each(function(){ $(this).html() != getTitle || $(this).parent().parent().addClass('active') });

	//////////////////////////////////////////// 3. Pop Over Init ///////////////////////////////////////////
	$('[data-toggle="popover"]').popover();

});