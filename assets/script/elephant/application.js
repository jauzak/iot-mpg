"use strict";
! function(a) {
	var b = {
		Constants: {
			MEDIA_QUERY_BREAKPOINT: "992px",
			TRANSITION_DELAY: 400,
			TRANSITION_DURATION: 400
		},
		CssClasses: {
			LAYOUT: "layout",
			LAYOUT_HEADER: "layout-header",
			LAYOUT_SIDEBAR: "layout-sidebar",
			LAYOUT_CONTENT: "layout-content",
			LAYOUT_FOOTER: "layout-footer",
			LAYOUT_HEADER_FIXED: "layout-header-fixed",
			LAYOUT_SIDEBAR_FIXED: "layout-sidebar-fixed",
			LAYOUT_FOOTER_FIXED: "layout-footer-fixed",
			LAYOUT_SIDEBAR_COLLAPSED: "layout-sidebar-collapsed",
			LAYOUT_SIDEBAR_STICKY: "layout-sidebar-sticky",
			SIDENAV: "sidenav",
			SIDENAV_BTN: "sidenav-toggler",
			SIDENAV_COLLAPSED: "sidenav-collapsed",
			SEARCH_FORM: "navbar-search",
			SEARCH_FORM_BTN: "navbar-search-toggler",
			SEARCH_FORM_COLLAPSED: "navbar-search-collapsed",
			CUSTOM_SCROLLBAR: "custom-scrollbar",
			CARD: "card",
			CARD_BODY: "card-body",
			CARD_COLLAPSED: "card-collapsed",
			CARD_FOCUSED: "card-focused",
			CARD_TOGGLER_BTN: "card-toggler",
			CARD_RELOAD_BTN: "card-reload",
			CARD_FOCUS_BTN: "card-focus",
			CARD_REMOVE_BTN: "card-remove",
			CARD_FOCUS_MODE: "card-focus-mode",
			THEME_PANEL: "theme-panel",
			THEME_PANEL_BTN: "theme-panel-toggler",
			THEME_PANEL_COLLAPSED: "theme-panel-collapsed",
			SPINNER: "spinner",
			SPINNER_PRIMARY: "spinner-primary",
			COLLAPSED: "collapsed"
		},
		Options: {
			CUSTOM_SCROLLBAR_BAR_CLASS: "custom-scrollbar-gripper",
			CUSTOM_SCROLLBAR_CLASS: "custom-scrollbar",
			CUSTOM_SCROLLBAR_DISTANCE: "5px",
			CUSTOM_SCROLLBAR_HEIGHT: "100%",
			CUSTOM_SCROLLBAR_POSITION: "right",
			CUSTOM_SCROLLBAR_RAIL_CLASS: "custom-scrollbar-track",
			CUSTOM_SCROLLBAR_SIZE: "6px",
			CUSTOM_SCROLLBAR_TOUCH_SCROLL_STEP: 50,
			CUSTOM_SCROLLBAR_WHEEL_STEP: 10,
			CUSTOM_SCROLLBAR_WIDTH: "100%",
			CUSTOM_SCROLLBAR_WRAPPER_CLASS: "custom-scrollable-area",
			SIDENAV_CLASS: "sidenav",
			SIDENAV_ACTIVE_CLASS: "open",
			SIDENAV_COLLAPSE_CLASS: "collapse",
			SIDENAV_COLLAPSE_IN_CLASS: "in",
			SIDENAV_COLLAPSING_CLASS: "collapsing",
			SELECT2_THEME: "bootstrap",
			SELECT2_WIDTH: "100%",
			STICKY: "sticky-scrollbar",
			STICKY_WRAPPER: "sticky-scrollable-area",
			STICKY_OFF_RESOLUTIONS: -768,
			STICKY_TOP: 55,
			BLOCK_UI_CSS_BACKGROUND_COLOR: "none",
			BLOCK_UI_CSS_BORDER: "none",
			BLOCK_UI_CSS_PADDING: 0,
			BLOCK_UI_OVERLAY_CSS_BACKGROUND_COLOR: "#fff",
			BLOCK_UI_OVERLAY_CSS_CURSOR: "wait",
			BLOCK_UI_OVERLAY_CSS_OPACITY: .8
		},
		KeyCodes: {
			S: 83,
			OPEN_SQUARE_BRACKET: 219,
			CLOSE_SQUARE_BRACKET: 221
		},
		init: function() {
			this.$document = a(document), this.$body = a(document.body), this.$layout = a("." + this.CssClasses.LAYOUT), this.$header = a("." + this.CssClasses.LAYOUT_HEADER), this.$sidebar = a("." + this.CssClasses.LAYOUT_SIDEBAR), this.$content = a("." + this.CssClasses.LAYOUT_CONTENT), this.$footer = a("." + this.CssClasses.LAYOUT_FOOTER), this.$scrollableArea = a("." + this.CssClasses.CUSTOM_SCROLLBAR), this.$sidenav = a("." + this.CssClasses.SIDENAV), this.$sidenavBtn = a("." + this.CssClasses.SIDENAV_BTN), this.$searchForm = a("." + this.CssClasses.SEARCH_FORM), this.$searchFormBtn = a("." + this.CssClasses.SEARCH_FORM_BTN), this.$cardTogglerBtn = a("." + this.CssClasses.CARD_TOGGLER_BTN), this.$cardReloadBtn = a("." + this.CssClasses.CARD_RELOAD_BTN), this.$cardFocusBtn = a("." + this.CssClasses.CARD_FOCUS_BTN), this.$cardRemoveBtn = a("." + this.CssClasses.CARD_REMOVE_BTN), this.$themePanel = a("." + this.CssClasses.THEME_PANEL), this.$themePanelBtn = a("." + this.CssClasses.THEME_PANEL_BTN), this.$themeSettings = this.$themePanel.find(":checkbox");
			var b = "(max-width: " + this.Constants.MEDIA_QUERY_BREAKPOINT + ")";
			this.mediaQueryList = window.matchMedia(b), this.mediaQueryMatches() && this.collapseSidenav(), this.addCustomScrollbarTo(this.$scrollableArea), this.initPlugins().bindEvents().syncThemeSettings()
		},
		bindEvents: function() {
			return this.$document.on("keydown.e.app", this.handleKeyboardEvent.bind(this)), this.$sidenavBtn.on("click.e.app", this.handleSidenavToggle.bind(this)), this.$sidenav.on("collapse-start.e.app", this.handleSidenavCollapseStart.bind(this)).on("expand-start.e.app", this.handleSidenavExpandStart.bind(this)), this.$sidenav.on("collapse-end.e.app", this.handleSidebarStickyUpdate.bind(this)).on("expand-end.e.app", this.handleSidebarStickyUpdate.bind(this)), this.$sidenav.on("shown.metisMenu.e.app", this.handleSidebarStickyUpdate.bind(this)).on("hidden.metisMenu.e.app", this.handleSidebarStickyUpdate.bind(this)), this.$searchFormBtn.on("click.e.app", this.handleSearchFormToggle.bind(this)), this.$cardTogglerBtn.on("click.e.app", this.handleCardToggle.bind(this)), this.$cardReloadBtn.on("click.e.app", this.handleCardReload.bind(this)), this.$cardFocusBtn.on("click.e.app", this.handleCardFocus.bind(this)), this.$cardRemoveBtn.on("click.e.app", this.handleCardRemove.bind(this)), this.$themePanelBtn.on("click.e.app", this.handleThemePanelToggle.bind(this)), this.$themeSettings.on("change.e.app", this.handleThemeSettingsChange.bind(this)), this.mediaQueryList.addListener(this.handleMediaQueryChange.bind(this)), this
		},
		handleKeyboardEvent: function(a) {
			if (!/input|textarea/i.test(a.target.tagName)) switch (a.keyCode) {
				case this.KeyCodes.S:
					this.toggleSearchForm();
					break;
				case this.KeyCodes.OPEN_SQUARE_BRACKET:
					this.toggleSidenav();
					break;
				case this.KeyCodes.CLOSE_SQUARE_BRACKET:
					this.toggleThemePanel()
			}
		},
		handleSidenavToggle: function(a) {
			a.preventDefault(), this.toggleSidenav()
		},
		handleSidenavCollapseStart: function(a) {
			var b = this.getThemeSettingsBy(this.CssClasses.LAYOUT_SIDEBAR_COLLAPSED);
			b.prop("checked", !0)
		},
		handleSidenavExpandStart: function(a) {
			var b = this.getThemeSettingsBy(this.CssClasses.LAYOUT_SIDEBAR_COLLAPSED);
			b.prop("checked", !1)
		},
		handleSidebarStickyUpdate: function(a) {
			this.isSidebarSticky() && this.updateStickySidebar()
		},
		handleSearchFormToggle: function(a) {
			a.preventDefault(), this.toggleSearchForm()
		},
		handleCardToggle: function(b) {
			this.$card = a(b.target).closest("." + this.CssClasses.CARD), this.$card.toggleClass(this.CssClasses.CARD_COLLAPSED).find("." + this.CssClasses.CARD_BODY).slideToggle(), this.$card.hasClass(this.CssClasses.CARD_COLLAPSED) ? (this.$card.attr("aria-expanded", !1), this.$cardTogglerBtn.attr("aria-expanded", !1).attr("title", "Expand")) : (this.$card.attr("aria-expanded", !0), this.$cardTogglerBtn.attr("aria-expanded", !0).attr("title", "Collapse")), b.preventDefault()
		},
		handleCardReload: function(b) {
			var c = a(b.target).closest("." + this.CssClasses.CARD),
				d = this.getBlockUIOptions();
			c.block(d), setTimeout(function() {
				c.unblock()
			}, 2e3)
		},
		handleCardFocus: function(b) {
			this.$body.toggleClass(this.CssClasses.CARD_FOCUS_MODE), a(b.target).closest("." + this.CssClasses.CARD).toggleClass(this.CssClasses.CARD_FOCUSED), b.preventDefault()
		},
		handleCardRemove: function(b) {
			this.$body.removeClass(this.CssClasses.CARD_FOCUS_MODE), a(b.target).closest("." + this.CssClasses.CARD).remove(), b.preventDefault()
		},
		handleThemePanelToggle: function(a) {
			a.preventDefault(), this.toggleThemePanel()
		},
		handleThemeSettingsChange: function(b) {
			var c = a(b.target);
			switch (c.attr("name")) {
				case this.CssClasses.LAYOUT_HEADER_FIXED:
					this.setHeaderFixed(c.prop("checked"));
					break;
				case this.CssClasses.LAYOUT_SIDEBAR_FIXED:
					this.setSidebarFixed(c.prop("checked"));
					break;
				case this.CssClasses.LAYOUT_SIDEBAR_STICKY:
					this.setSidebarSticky(c.prop("checked"));
					break;
				case this.CssClasses.LAYOUT_SIDEBAR_COLLAPSED:
					this.$sidenavBtn.trigger("click");
					break;
				case this.CssClasses.LAYOUT_FOOTER_FIXED:
					this.setFooterFixed(c.prop("checked"))
			}
		},
		handleMediaQueryChange: function(a) {
			this[this.mediaQueryMatches() ? "collapseSidenav" : "expandSidenav"]()
		},
		collapseSidenav: function() {
			var b = a.Event("collapse-start");
			this.$layout.addClass(this.CssClasses.LAYOUT_SIDEBAR_COLLAPSED), this.$sidenav.trigger(b).css("opacity", 0), this.$sidenav.addClass(this.CssClasses.SIDENAV_COLLAPSED), this.$sidenavBtn.addClass(this.CssClasses.COLLAPSED), this.transitionTimeoutId && clearTimeout(this.transitionTimeoutId), this.transitionTimeoutId = setTimeout(function() {
				this.$sidenav.animate({
					opacity: 1
				}).trigger("collapse-end")
			}.bind(this), this.Constants.TRANSITION_DELAY), this.$sidenav.attr("aria-expanded", !1), this.$sidenavBtn.attr("aria-expanded", !1).attr("title", "Expand sidenav ( [ )")
		},
		expandSidenav: function() {
			var b = a.Event("expand-start");
			this.$layout.removeClass(this.CssClasses.LAYOUT_SIDEBAR_COLLAPSED), this.$sidenav.trigger(b).css("opacity", 0), this.$sidenav.removeClass(this.CssClasses.SIDENAV_COLLAPSED), this.$sidenavBtn.removeClass(this.CssClasses.COLLAPSED), this.transitionTimeoutId && clearTimeout(this.transitionTimeoutId), this.transitionTimeoutId = setTimeout(function() {
				this.$sidenav.animate({
					opacity: 1
				}).trigger("expand-end")
			}.bind(this), this.Constants.TRANSITION_DELAY), this.$sidenav.attr("aria-expanded", !0), this.$sidenavBtn.attr("aria-expanded", !0).attr("title", "Collapse sidenav ( [ )")
		},
		toggleSidenav: function() {
			this[this.isSidenavCollapsed() ? "expandSidenav" : "collapseSidenav"]()
		},
		isSidenavCollapsed: function() {
			return this.$sidenav.hasClass(this.CssClasses.SIDENAV_COLLAPSED)
		},
		toggleSearchForm: function() {
			this.$searchForm.toggleClass(this.CssClasses.SEARCH_FORM_COLLAPSED), this.$searchFormBtn.toggleClass(this.CssClasses.COLLAPSED), this.isSearchFormCollapsed() ? (this.$searchForm.attr("aria-expanded", !1), this.$searchFormBtn.attr("aria-expanded", !1).attr("title", "Expand search form ( S )")) : (this.$searchForm.attr("aria-expanded", !0), this.$searchFormBtn.attr("aria-expanded", !0).attr("title", "Collapse search form ( S )"))
		},
		isSearchFormCollapsed: function() {
			return this.$searchForm.hasClass(this.CssClasses.SEARCH_FORM_COLLAPSED)
		},
		toggleThemePanel: function() {
			this.$themePanel.toggleClass(this.CssClasses.THEME_PANEL_COLLAPSED), this.$themePanelBtn.toggleClass(this.CssClasses.COLLAPSED), this.isThemePanelCollapsed() ? (this.$themePanel.attr("aria-expanded", !1), this.$themePanelBtn.attr("aria-expanded", !1).attr("title", "Expand theme panel ( ] )")) : (this.$themePanel.attr("aria-expanded", !0), this.$themePanelBtn.attr("aria-expanded", !0).attr("title", "Collapse theme panel ( ] )"))
		},
		isThemePanelCollapsed: function() {
			return this.$themePanel.hasClass(this.CssClasses.THEME_PANEL_COLLAPSED)
		},
		syncThemeSettings: function() {
			var b = {};
			return this.$themeSettings.each(function(c, d) {
				var e = a(d),
					f = e.attr("name");
				e.data("sync") && (b[f] = this.$layout.hasClass(f))
			}.bind(this)), this.changeThemeSettings(b), this
		},
		changeThemeSettings: function(b) {
			return a.each(b, function(a, b) {
				var c = this.getThemeSettingsBy(a);
				c.prop("checked", b).trigger("change")
			}.bind(this)), this
		},
		getThemeSettingsBy: function(a) {
			return this.$themeSettings.filter("[name='" + a + "']")
		},
		isHeaderStatic: function() {
			return !this.$layout.hasClass(this.CssClasses.LAYOUT_HEADER_FIXED)
		},
		setHeaderFixed: function(a) {
			var b = {};
			this.$layout.toggleClass(this.CssClasses.LAYOUT_HEADER_FIXED, a), this.isHeaderStatic() && this.isSidebarFixed() && (b[this.CssClasses.LAYOUT_SIDEBAR_FIXED] = a), this.isHeaderStatic() && this.isSidebarSticky() && (b[this.CssClasses.LAYOUT_SIDEBAR_STICKY] = a), this.changeThemeSettings(b)
		},
		isSidebarFixed: function() {
			return this.$layout.hasClass(this.CssClasses.LAYOUT_SIDEBAR_FIXED)
		},
		setSidebarFixed: function(a) {
			var b = {},
				c = this.getSidebarScrollableArea();
			return this.$layout.toggleClass(this.CssClasses.LAYOUT_SIDEBAR_FIXED, a), this.isSidebarFixed() ? (this.isHeaderStatic() && (b[this.CssClasses.LAYOUT_HEADER_FIXED] = a), this.isSidebarSticky() && (b[this.CssClasses.LAYOUT_SIDEBAR_STICKY] = !a), void this.changeThemeSettings(b).addCustomScrollbarTo(c)) : this.removeCustomScrollbarFrom(c)
		},
		isSidebarSticky: function() {
			return this.$layout.hasClass(this.CssClasses.LAYOUT_SIDEBAR_STICKY)
		},
		setSidebarSticky: function(a) {
			var b = {};
			return this.$layout.toggleClass(this.CssClasses.LAYOUT_SIDEBAR_STICKY, a), this.isSidebarSticky() ? (this.isHeaderStatic() && (b[this.CssClasses.LAYOUT_HEADER_FIXED] = a), this.isSidebarFixed() && (b[this.CssClasses.LAYOUT_SIDEBAR_FIXED] = !a), void this.changeThemeSettings(b).createStickySidebar()) : this.destroyStickySidebar()
		},
		setFooterFixed: function(a) {
			this.$layout.toggleClass(this.CssClasses.LAYOUT_FOOTER_FIXED, a)
		},
		addCustomScrollbarTo: function(a) {
			var b = this.getCustomScrollbarOptions();
			a.slimScroll(b)
		},
		removeCustomScrollbarFrom: function(a) {
			var b = this.getCustomScrollbarOptions();
			b.destroy = !0, a.slimScroll(b).off().removeAttr("style")
		},
		createStickySidebar: function() {
			var a = this.getSidebarScrollableArea(),
				b = this.getStickyOptions();
			b.stickTo = this.$content, a.hcSticky(b).hcSticky("reinit")
		},
		updateStickySidebar: function() {
			var a = this.getSidebarScrollableArea();
			a.hcSticky("reinit")
		},
		destroyStickySidebar: function() {
			var a = this.getSidebarScrollableArea();
			a.data("hcSticky") && a.hcSticky("destroy").off().removeAttr("style")
		},
		mediaQueryMatches: function() {
			return this.mediaQueryList.matches
		},
		getSidebarScrollableArea: function() {
			return this.$sidebar.find("." + this.CssClasses.CUSTOM_SCROLLBAR)
		},
		getCreateOptions: function(b) {
			var c = new RegExp("^" + b + "(_)?", "i"),
				d = {};
			return a.each(this.Options, function(b, e) {
				c.test(b) && (b = b.replace(c, "").replace(/_/g, "-"), b = a.camelCase(b.toLowerCase()), d[b] = e)
			}), d
		},
		getCustomScrollbarOptions: function() {
			return this.getCreateOptions("custom_scrollbar")
		},
		getSelect2Options: function() {
			return this.getCreateOptions("select2")
		},
		getSidenavOptions: function() {
			return this.getCreateOptions("sidenav")
		},
		getStickyOptions: function() {
			return this.getCreateOptions("sticky")
		},
		getSortableOptions: function() {
			return this.getCreateOptions("sortable")
		},
		getBlockUIOptions: function() {
			var b = {},
				c = a(document.createElement("div"));
			return c.addClass(this.CssClasses.SPINNER).addClass(this.CssClasses.SPINNER_PRIMARY), b.message = c, b.css = this.getCreateOptions("block_ui_css"), b.overlayCSS = this.getCreateOptions("block_ui_overlay_css"), b
		},
		initPlugins: function() {
			return this.initPeity(), this.matchHeight(), this.metisMenu(), this.select2(), this.tooltip(), this.vectorMap(), this
		},
		initPeity: function() {
			a("[data-peity]").each(function() {
				var b = a(this).data(),
					c = a.camelCase(b.peity);
				a(this).peity(c, b)
			})
		},
		matchHeight: function() {
			a('[data-toggle="match-height"]').matchHeight()
		},
		metisMenu: function() {
			var a = this.getSidenavOptions();
			this.$sidenav.metisMenu(a)
		},
		select2: function() {
			var b = a.fn.select2,
				c = this.getSelect2Options();
			a.each(c, function(a, c) {
				b.defaults.set(a, c)
			})
		},
		tooltip: function() {
			a('[data-toggle="tooltip"]').tooltip()
		},
		vectorMap: function() {
			a('[data-toggle="vector-map"]').each(function() {
				var b = a(this),
					c = b.data();
				b.vectorMap(c)
			})
		}
	};
	b.init()
}(jQuery);