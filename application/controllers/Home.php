<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->template->load('layout/content', 'user/dashboard/home');
    }

    public function dashboard(){
        $this->template->load('layout/content', 'user/dashboard/dashboard');
    }

    public function offline(){
        $this->template->load('layout/content-offline', 'user/dashboard/offline');
    }

    public function firebase(){
        $this->template->load('layout/content', 'user/dashboard/firebase');
    }
}
