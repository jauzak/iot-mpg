<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Rest extends CI_Controller {
    public function __construct(){
        parent::__construct();
        header('Content-Type: application/json');
    }

    public function get_node_wifi(){
        $sql = "SELECT * FROM `terimadata` WHERE node = ? ORDER BY `ts2` DESC LIMIT 10";
        $query = $this->db->query($sql, array('WiFi'));
        $result = $query->result();
        $wifi_node = [];

        foreach ($result as $value)
        array_push($wifi_node, $value->value2);

        exit(json_encode(array_reverse($wifi_node)));
    }

    public function get_node_ble(){
        $sql = "SELECT * FROM `terimadata` WHERE node = ? ORDER BY `ts2` DESC LIMIT 10";
        $query = $this->db->query($sql, array('BLE'));
        $result = $query->result();
        $ble_node = [];

        foreach ($result as $value)
        array_push($ble_node, $value->value2);

        exit(json_encode(array_reverse($ble_node)));
    }

    public function get_node_zbee(){
        $sql = "SELECT * FROM `terimadata` WHERE node = ? ORDER BY `ts2` DESC LIMIT 10";
        $query = $this->db->query($sql, array('XBEE'));
        $result = $query->result();
        $ble_node = [];

        foreach ($result as $value)
        array_push($ble_node, $value->value2);

        exit(json_encode(array_reverse($ble_node)));
    }

    public function get_node_lora(){
        $sql = "SELECT * FROM `terimadata` WHERE node = ? ORDER BY `ts2` DESC LIMIT 10";
        $query = $this->db->query($sql, array('LORA'));
        $result = $query->result();
        $lora_node = [];

        foreach ($result as $value)
        array_push($lora_node, $value->value2);

        exit(json_encode(array_reverse($lora_node)));
    }

    public function get_table_log(){
        $sql = "SELECT * FROM `terimadata` ORDER BY `ts2` DESC LIMIT 100";
        $query = $this->db->query($sql);
        $result = $query->result();

        exit(json_encode($result));
    }

}
