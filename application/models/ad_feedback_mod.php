<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ad_feedback_mod extends CI_Model {
	
	function Ad_feedback_mod(){
		parent::__construct();
	}
	
	function getNumRow($val){
		if($val!=''){
			$data = array('id'=>$val);
			$this->db->where($data);
		}
		$this->db->order_by('tanggal');
		$query = $this->db->get('forum');
		
		$result = $query->num_rows();
		return $result;
	}
	
	function manage($offset,$limit){
		$this->db->order_by('tanggal','desc');
		$this->db->limit($limit, $offset);
		$query = $this->db->get('forum');
		
		$result = $query->result();
		return $result;
	}
	
	function getPost($val){
		$data = array('id'=>$val);
		$this->db->where($data);
		$query = $this->db->get('forum');
		
		$result =  $query->result();
		return $result;
	}
	
	function create($val){
		$data = array(
					'username'=>$val['username'],
					'full_name'=>$val['full_name'],
					'url'=>$val['url'],
					'email'=>$val['email'],
					'role'=>$val['role'],
					'url'=>$val['url'],
					'password'=>md5($val['password1']),
					'status'=>$val['status']
				);
		$str = $this->db->insert_string('user', $data);
		$this->db->query($str);
	}
	
	function edit($val){
		$data = array(
					'full_name'=>$val['full_name'],
					'url'=>$val['url'],
					'email'=>$val['email'],
					'role'=>$val['role'],
					'url'=>$val['url'],
					'status'=>$val['status']
				);
		if ($val['password1']!=''){
			$data += array('password'=>md5($val['password1']));	
		}
		$this->db->where('username', $val['username']);
		$this->db->update('user', $data); 
	}
	
	function editing($val,$sts){
		$data = array('status' => $sts);
		$this->db->where('id_forum', $val);
		$this->db->update('forum', $data); 
	}
	
	function delete($val){
		$this->db->where('username',$val);
		$this->db->delete('user'); 
	}

}
?>