<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="Multi Protocol Gateway">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:url" content="http://iot-mpg.com">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Multi Protocol Gateway">
    <meta property="og:description" content="Multi Protocol Gateway">

    <link rel="icon" type="image/png" href="<?= base_url('assets/images/logo.png') ?>" sizes="16x16">
    <link rel='manifest' href="<?= base_url('manifest.webmanifest') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/elephant/fonts.google.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/elephant/vendor.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/elephant/elephant.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/elephant/application.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/custom/custom.css') ?>">

    <script>function base_url(url = '') {return "<?= base_url() ?>" + url}</script>
    <script src="<?= base_url('assets/script/elephant/vendor.js') ?>"></script>
  </head>
  <body class="layout layout-header-fixed layout-sidebar-fixed layout-footer-fixed" >
    <div class="layout-header">
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <a class="navbar-brand navbar-brand-center" href="<?= base_url() ?>"><p>IoT - Multi Protocol Gateway</p></a>
        </div>
      </div>
    </div>
    <div class="layout-main">
      <div class="layout-content layout-nomargin">
        <?= $contents ?>
      </div>
      <div class="layout-footer layout-nomargin">
        <div class="layout-footer-body">
          <small class="copyright">Multi Protocol Gateway</small>
        </div>
      </div>
    </div>
  </body>
</html>
