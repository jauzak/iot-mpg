<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="Multi Protocol Gateway">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:url" content="http://iot-mpg.com">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Multi Protocol Gateway">
    <meta property="og:description" content="Multi Protocol Gateway">

    <link rel='manifest' href="<?= base_url('manifest.webmanifest') ?>">
    <link rel="apple-touch-icon" sizes="192x192" href="<?= base_url('assets/images/logo.png') ?>">
    <link rel="icon" type="image/png" href="<?= base_url('assets/images/logo.png') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/elephant/fonts.google.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/elephant/vendor.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/elephant/elephant.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/elephant/application.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/custom/custom.css') ?>">

    <script>function base_url(url = '') {return "<?= base_url() ?>" + url}</script>
    <script src="<?= base_url('assets/script/elephant/vendor.js') ?>"></script>
    <script src="<?= base_url('assets/script/elephant/angular.min.js') ?>"></script>
    <script src="<?= base_url('assets/script/custom/custom.js') ?>"></script>
  </head>
  <body class="layout layout-header-fixed layout-sidebar-fixed layout-footer-fixed" >
    <div class="layout-header">
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <a class="navbar-brand navbar-brand-center" href="<?= base_url() ?>"><p>IoT - Multi Protocol Gateway</p></a>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
          </button>

        </div>
        <div class="navbar-toggleable">
          <nav id="navbar" class="navbar-collapse collapse">
            <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
            </button>
            
          </nav>
        </div>
      </div>
    </div>
    <div class="layout-main">
      <div class="layout-sidebar">
        <div class="layout-sidebar-body">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">

              <ul class="sidenav">
                <li class="sidenav-heading">Navigation</li>
                <li class="sidenav-item">
                  <a href="<?= base_url() ?>">
                    <span class="sidenav-icon icon icon-home"></span>
                    <span class="sidenav-label">Home</span>
                  </a>
                </li>
                <li class="sidenav-item">
                  <a href="<?= base_url('home/dashboard') ?>">
                    <span class="sidenav-icon icon icon-gear"></span>
                    <span class="sidenav-label">Dashboard</span>
                  </a>
                </li>
              </ul>

            </nav>
          </div>
        </div>
      </div>
      <div class="layout-content">
        <?= $contents ?>
      </div>
      <div class="layout-footer">
        <div class="layout-footer-body">
          <small class="copyright">Multi Protocol Gateway</small>
        </div>
      </div>
    </div>
    <noscript>
      <div style='background: #000; position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; color: #fff; display: table; z-index: 9999;'>
          <div style='display:table-cell; vertical-align: middle; text-align:center;'>
              <h3>Please enable your javascript while using this site</h3>
          </div>
      </div>
    </noscript>
    <script src="<?= base_url('assets/script/elephant/elephant.js') ?>"></script>
    <script src="<?= base_url('assets/script/elephant/application.js') ?>"></script>
    <script>
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register(base_url('sw.js')).then(
          function() { console.log('CLIENT: service worker registration complete.'); }, 
          function() { console.log('CLIENT: service worker registration failure.'); });
      
      }else{
        console.log('CLIENT: service worker is not supported.');
      }
    </script>
  </body>
</html>
