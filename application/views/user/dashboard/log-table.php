<tr>
	<td>1</td>
	<td>BLE</td>
	<td>Temperature</td>
	<td>124</td>
	<td>20 Juli 2019</td>
</tr>

<?php foreach ($kuesioner_universitas as $i => $row) : ?>
  <tr>
    <td><?= ++$i ?></td>
    <td><?= strtolower($row->user) ?></td>
    <td><?= date('d M Y', strtotime($row->update_at)) ?></td>
    <td><?= $row->nama ?></td>
    <td><?= $row->position ?></td>
    <td><?= $row->email ?></td>
    <td><?= $row->is_submit == '1' ? 'Sudah' : 'Belum' ?></td>  
    <td align="center">
      <span data-toggle="tooltip" data-placement="left" title="Detail">
          <button 
            class       = "btn btn-xs btn-info btn-kuesioner-detail"
            data-toggle = "modal"
            data-target = ".modal-kuesioner-detail"
            data-user   = '<?= $row->user ?>'
            data-tahun  = '2018'
            ><i class="icon icon-eye"></i>
          </button>
      </span>
      <span data-toggle="tooltip" data-placement="left" title="Cetak"> 
          <button class="btn btn-xs btn-primary btn-kuesioner-print" data-user='<?= $row->user ?>'><i class="icon icon-print"></i></button>
      </span>
    </td>
  </tr>
<?php endforeach ?>