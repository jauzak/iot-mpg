<div class="layout-content-body" ng-app="iotmpg">
  <div class="title-bar">
    <div class="media-left">
      <span class="icon icon-gear bg-gray rounded sq-48"></span>
    </div>
    <div class="media-middle media-body">
      <h1 class="title-bar-title">
        <span class="d-ib">Dashboard</span>
      </h1>
      <ul class="breadcrumb">
        <li><a href="<?= base_url() ?>"><i class="icon icon-home"></i></a></li>
        <li><a href="<?= base_url('home/dashboard') ?>">Dashboard</a></li>
      </ul>
    </div>
  </div>
  <div ng-controller="dashboard">
    <div class="row gutter-xs">
      <div class="col-xs-6 col-md-3">
        <div class="card bg-info no-border">
          <div class="card-values">
            <div class="p-x">
              <small>WiFi Protocol</small> 
              <h3 class="card-title fw-l wifi-latest-data">NaN</h3>
            </div>
          </div>
          <div class="card-chart" style="max-height: 40px; height: 40px">
            <canvas id="wifi-chart" height="40"></canvas>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-md-3">
        <div class="card bg-primary no-border">
          <div class="card-values">
            <div class="p-x">
              <small>Bluetooth Protocol</small>
              <h3 class="card-title fw-l ble-latest-data">NaN</h3>
            </div>
          </div>
          <div class="card-chart" style="max-height: 40px; height: 40px">
            <canvas id="ble-chart" height="40"></canvas>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-md-3">
        <div class="card bg-info no-border">
          <div class="card-values">
            <div class="p-x">
              <small>ZigBee Protocol</small>
              <h3 class="card-title fw-l zbee-latest-data">NaN</h3>
            </div>
          </div>
          <div class="card-chart" style="max-height: 40px; height: 40px">
            <canvas id="zbee-chart" height="40"></canvas>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-md-3">
        <div class="card bg-primary no-border">
          <div class="card-values">
            <div class="p-x">
              <small>LoRa Protocol</small>
              <h3 class="card-title fw-l lora-latest-data">NaN</h3>
            </div>
          </div>
          <div class="card-chart" style="max-height: 40px; height: 40px">
            <canvas id="lora-chart" height="40"></canvas>
          </div>
        </div>
      </div>
    </div>
    <div class="row gutter-xs">
      <div class="col-xs-12">
        <div class="card">
          <div class="card-header">
            <div class="card-actions">
              <button type="button" class="card-action card-toggler" title="Collapse"></button>
              <button type="button" class="card-action card-reload" title="Reload"></button>
              <button type="button" class="card-action card-remove" title="Remove"></button>
            </div>
            <strong>Log Table</strong>
          </div>
          <div class="card-body">
            <table class="log-table table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="text-center" width="5%">#</th>
                  <th>Node</th>
                  <th>Attribute</th>
                  <th>Value</th>
                  <th>Timestamp</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="value in logdata">
                  <td align="center">{{ $index + 1 }}</td>
                  <td ng-switch on="value.id_node">
                    <p ng-switch-when='1'>WiFi</p>
                    <p ng-switch-when='2'>ZigBee</p>
                    <p ng-switch-when='3'>Bluetooth Low Energy</p>
                    <p ng-switch-when='4'>LoRa</p>
                  </td>
                  <td>{{ value.topic2 }}</td>
                  <td>{{ value.value2 }}</td>
                  <td>{{ value.ts2 }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
var options = {
    animation: { duration: 0 },
    elements: { point:{ radius: 0 }},
    legend: { display: false },
    scales: { yAxes: [{ ticks: {display: false}, gridLines: {drawBorder: false, display: false }}], xAxes: [{ gridLines: {drawBorder: false, display: false }}]
    }};

var labels = ['', '', '', '', '', '', '', '', '', ''],
    borderColor = ['rgba(255, 255, 255, 1)'],
    backgroundColor = ['rgba(0, 0, 0, 0)'],
    borderWidth = 2;

var IoTMPG = angular.module('iotmpg', []);

IoTMPG.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

IoTMPG.controller('dashboard', function($scope, $http){ 
  // Loading Bar
  $('.log-table thead tr').after('<tr class="loading-bar"><td colspan="5" align="center"><i class="icon icon-spin icon-refresh"></i></td></tr>');

  $scope.getWiFiNode = function(){
    $http.get(base_url('rest/get_node_wifi')).then(function(response){
      $scope.wifidata = response.data;
      $('.wifi-latest-data').html($scope.wifidata[Object.keys($scope.wifidata).length-1] + '&deg;C');
      var wifi = new Chart(document.getElementById('wifi-chart').getContext('2d'), {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
              data: $scope.wifidata,
              borderColor: borderColor,
              backgroundColor: backgroundColor,
              borderWidth: borderWidth
            }]
        },
        options: options
      });
    })
  };

  $scope.getBlueNode = function(){
    $http.get(base_url('rest/get_node_ble')).then(function(response){
      $scope.bledata = response.data;
      $('.ble-latest-data').html($scope.bledata[Object.keys($scope.bledata).length-1] + '&deg;C');
      var ble = new Chart(document.getElementById('ble-chart').getContext('2d'), {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
              data: $scope.bledata,
              borderColor: borderColor,
              backgroundColor: backgroundColor,
              borderWidth: borderWidth
            }]
        },
        options: options
      });
    })
  };

  $scope.getZbeeNode = function(){
    $http.get(base_url('rest/get_node_zbee')).then(function(response){
      $scope.zbeedata = response.data;
      $('.zbee-latest-data').html($scope.zbeedata[Object.keys($scope.zbeedata).length-1] + '&deg;C');
      var zbee = new Chart(document.getElementById('zbee-chart').getContext('2d'), {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
              data: $scope.zbeedata,
              borderColor: borderColor,
              backgroundColor: backgroundColor,
              borderWidth: borderWidth
            }]
        },
        options: options
      });
    })
  };

  $scope.getLoRaNode = function(){
    $http.get(base_url('rest/get_node_lora')).then(function(response){
      $scope.loradata = response.data;
      $('.lora-latest-data').html($scope.loradata[Object.keys($scope.loradata).length-1] + '&deg;C');
      var lora = new Chart(document.getElementById('lora-chart').getContext('2d'), {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
              data: $scope.loradata,
              borderColor: borderColor,
              backgroundColor: backgroundColor,
              borderWidth: borderWidth
            }]
        },
        options: options
      });
    })
  };

  $scope.getTableLog = function(){
    $http.get(base_url('rest/get_table_log')).then(function(response){
      $scope.logdata = response.data;
      var time = new Date($.now());
      
      $('.log-table thead .loading-bar').remove();
      $('.copyright').html('Latest data fetch: ' + time)
      
    }, function(){
      $('.log-table thead .loading-bar').remove();
      toastr.error('Unreachable &#128546;', 'Tabel Log');
    })
  };

  $scope.timer = 1 * 1 * 1000; // minutes * seconds * miliseconds
  setInterval(function(){$scope.getWiFiNode()}, $scope.timer);
  setInterval(function(){$scope.getBlueNode()}, $scope.timer);
  setInterval(function(){$scope.getZbeeNode()}, $scope.timer);
  setInterval(function(){$scope.getLoRaNode()}, $scope.timer);
  setInterval(function(){$scope.getTableLog()}, $scope.timer);
});


// Wifi Node
// var get_wifi_node = setInterval( function(){
//   $.ajax({
//     type: 'GET',
//     url: base_url('home/get_node_wifi'),
//     beforeSend: function(){},
//     success: function(data){
//       latest_data = data[0]
//       $('.wifi-latest-data').html(latest_data + '&deg;C')
//       var wifi = new Chart(document.getElementById('wifi-chart').getContext('2d'), {
//         type: 'line',
//         data: {
//             labels: labels,
//             datasets: [{
//               data: data,
//               borderColor: borderColor,
//               backgroundColor: backgroundColor,
//               borderWidth: borderWidth
//             }]
//         },
//         options: options
//       });
//     },
//     error: function(){}
//   });
// }, 1 * 2* 1000 ); // minutes * seconds * miliseconds

// BLE Node
// var get_ble_node = setInterval( function(){
//   $.ajax({
//     type: 'GET',
//     url: base_url('home/get_node_ble'),
//     beforeSend: function(){},
//     success: function(data){
//       latest_data = data[0]
//       $('.ble-latest-data').html(latest_data + '&deg;C')
//       var wifi = new Chart(document.getElementById('ble-chart').getContext('2d'), {
//         type: 'line',
//         data: {
//             labels: labels,
//             datasets: [{
//               data: data,
//               borderColor: borderColor,
//               backgroundColor: backgroundColor,
//               borderWidth: borderWidth
//             }]
//         },
//         options: options
//       });
//     },
//     error: function(){}
//   });
// }, 1 * 2 * 1000 ); // minutes * seconds * miliseconds

// ZBEE Node
// var get_zbee_node = setInterval( function(){
//   $.ajax({
//     type: 'GET',
//     url: base_url('home/get_node_zbee'),
//     beforeSend: function(){},
//     success: function(data){
//       latest_data = data[0]
//       $('.zbee-latest-data').html(latest_data + '&deg;C')
//       var wifi = new Chart(document.getElementById('zbee-chart').getContext('2d'), {
//         type: 'line',
//         data: {
//             labels: labels,
//             datasets: [{
//               data: data,
//               borderColor: borderColor,
//               backgroundColor: backgroundColor,
//               borderWidth: borderWidth
//             }]
//         },
//         options: options
//       });
//     },
//     error: function(){}
//   });
// }, 1 * 2 * 1000 ); // minutes * seconds * miliseconds
</script>