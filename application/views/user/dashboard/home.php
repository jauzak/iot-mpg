<div class="layout-content-body">
  <div class="title-bar">
    <div class="media-left">
      <span class="icon icon-home bg-gray rounded sq-48"></span>
    </div>
    <div class="media-middle media-body">
      <h1 class="title-bar-title">
        <span class="d-ib">Home</span>
      </h1>
      <ul class="breadcrumb">
        <li><a href="<?= base_url() ?>"><i class="icon icon-home"></i></a></li>
        <li><a href="<?= base_url() ?>">Home</a></li>
      </ul>
    </div>
  </div>
  <div class="row gutter-xs">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          <div class="card-actions">
            <button type="button" class="card-action card-toggler" title="Collapse"></button>
            <button type="button" class="card-action card-reload" title="Reload"></button>
            <button type="button" class="card-action card-remove" title="Remove"></button>
          </div>
          <strong>About</strong>
        </div>
        <div class="card-body">
          <p>The Internet of Things (IoT) is a system of interrelated computing devices, mechanical and digital machines, objects, animals or people that are provided with unique identifiers (UIDs) and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction.</p>
          <p>A Multi-Protocol Gateway can accept client-originated messages in various protocols. The service can then pass messages to a remote server with various protocols. The protocol that the client uses does not need to be the same as the protocol that the remote server uses.</p>
          <p>In this project we create multi protocol nodes (WiFI, BLE, Zigbee, LoRa) of temperature censors which will be pooled in a relational database based on arduino device. Later the pool will be sent into relational database on online server. The data are then served on this web-based dashboard.</p>
        </div>
      </div>
    </div>
  </div>

</div>
