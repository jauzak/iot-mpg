<div class="layout-content-body" ng-app="iotmpg">
  <div class="title-bar">
    <div class="media-left">
      <span class="icon icon-gear bg-gray rounded sq-48"></span>
    </div>
    <div class="media-middle media-body">
      <h1 class="title-bar-title">
        <span class="d-ib">Dashboard</span>
      </h1>
      <ul class="breadcrumb">
        <li><a href="<?= base_url() ?>"><i class="icon icon-home"></i></a></li>
        <li><a href="<?= base_url('home/firebase') ?>">Firebase</a></li>
      </ul>
    </div>
  </div>
  <div ng-controller="dashboard">
    <div class="row gutter-xs">
      <div class="col-xs-12">
        <div class="card">
          <div class="card-header">
            <div class="card-actions">
              <button type="button" class="card-action card-toggler" title="Collapse"></button>
              <button type="button" class="card-action card-reload" title="Reload"></button>
              <button type="button" class="card-action card-remove" title="Remove"></button>
            </div>
            <strong>Log Table</strong>
          </div>
          <div class="card-body">
            <div class="form-group">
              <!-- <label class="col-md-3 control-label" for="form-control-1">Text input</label> -->
              <div class="col-md-12">
                <input class="form-control fb-input" type="text" value="This is a text">
              </div>
              <div class="col-md-12" style="margin-top: 10px">
                <button class="btn btn-md btn-primary fb-btn">CLICK IT</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/6.6.0/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#config-web-app -->

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyACSXGJbYh-AFF-3AiLIkdZY277wx7e2ZA",
    authDomain: "iot-mpg.firebaseapp.com",
    databaseURL: "https://iot-mpg.firebaseio.com",
    projectId: "iot-mpg",
    storageBucket: "",
    messagingSenderId: "631256350942",
    appId: "1:631256350942:web:572e014ded022268dec24f"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  // alert($('.fb-input').val())


</script>