<div class="layout-content-body">
  <div class="row gutter-xs">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          <strong>Status: Offline</strong>
        </div>
        <div class="card-body">
          <p>You are now off the line. Please check your internet connection and refresh the application again.</p>
      </div>
    </div>
    <a class="btn btn-primary btn-md" style="width: 100%" href="<?= base_url() ?>">Refresh</a>
  </div>

</div>
