<?php
function linechart($ydata, $title='Line Chart')
{
    require_once("jpgraph/jpgraph.php");
    require_once("jpgraph/jpgraph_line.php");    
    
    // Create the graph. These two calls are always required
    $graph = new Graph(350,250,"auto",60);
    $graph->SetScale("textlin");
    
    // Setup title
    $graph->title->Set($title);
    
    // Create the linear plot
    $lineplot=new LinePlot($ydata);
    $lineplot->SetColor("blue");
    
    // Add the plot to the graph
    $graph->Add($lineplot);
    
    return $graph; // does PHP5 return a reference automatically?
}


function barchart($ydata, $title='Bar Chart')
{
    require_once("jpgraph/jpgraph.php");
    require("jpgraph/jpgraph_bar.php");    
    
    // Create the graph. These two calls are always required
    $graph = new Graph(1000,1000,"auto",60);
	//$graph = new Graph(350, 250, 'auto', 0, false);
	//$graph->img->SetMargin(30, 30, 30, 30);     
    $graph->SetScale("textlin");
    
    // Setup title
    $graph->title->Set($title);
    
    // Create the linear plot
    $barplot=new BarPlot($ydata);
    $barplot->SetColor("blue");
    
    // Add the plot to the graph
    $graph->Add($barplot);
    
    return $graph; // does PHP5 return a reference automatically?
}

function piechart($ydata, $title='Bar Chart')
{
    require_once("jpgraph/jpgraph.php");
    require("jpgraph/jpgraph_pie.php");    
    
    // Create the graph. These two calls are always required
    //$graph = new Graph(350,250,"auto",60);
	$graph = new Graph(350, 250, 'auto', 0, false);
	//$graph->img->SetMargin(30, 30, 30, 30);     
    $graph->SetScale("textlin");
    
    // Setup title
    $graph->title->Set($title);
    
    // Create the linear plot
    $barplot=new PiePlot($ydata);
    $barplot->SetColor("blue");
    
    // Add the plot to the graph
    $graph->Add($barplot);
    
    return $graph; // does PHP5 return a reference automatically?
}
?>  