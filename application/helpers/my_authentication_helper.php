<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    function authorized(){
		$CI =& get_instance();
		$CI->load->library('session');

		$uname = $CI->session->userdata('uname');
		$role  = $CI->session->userdata('role');
		$site  = $CI->session->userdata('site');
		
		return ($uname != NULL || $role != NULL || $site != NULL) && ($site == base_url()) ? TRUE : FALSE;
    }

    function is_university(){
		$CI =& get_instance();
		$CI->load->library('session');

		return authorized() && $CI->session->userdata('role') == '0' && !strpos($CI->session->userdata('uname'), 'demo') ? TRUE : FALSE;
    }

    function is_demo(){
		$CI =& get_instance();
		$CI->load->library('session');

		return authorized() && $CI->session->userdata('role') == '0' && strpos($CI->session->userdata('uname'), 'demo') ? TRUE : FALSE;
    }

    function is_faculty(){
		$CI =& get_instance();
		$CI->load->library('session');

		return authorized() && $CI->session->userdata('role') == '1' ? TRUE : FALSE;
    }

    function is_reviewer(){
		$CI =& get_instance();
		$CI->load->library('session');

		return authorized() && $CI->session->userdata('role') == '2' ? TRUE : FALSE;
    }

    function is_faculty_reviewer(){
		$CI =& get_instance();
		$CI->load->library('session');

		return $CI->session->userdata('uname') == 'reviewer1' || $CI->session->userdata('uname') == 'reviewer2' || $CI->session->userdata('uname') == 'reviewer3' || $CI->session->userdata('uname') == 'reviewer4' || $CI->session->userdata('uname') == 'reviewer5' || $CI->session->userdata('uname') == 'reviewer30' ? TRUE : FALSE;
    }

    function is_admin(){
		$CI =& get_instance();
		$CI->load->library('session');

		return authorized() && $CI->session->userdata('role') == md5('administrator') ? TRUE : FALSE;
    }

    function is_whitelist(){
    	// You might want to put ISP or your machine name inside this array (use trim(`hostname`) to know your hostname)
		// If you prefer using $ipaddress than using $ispname, add '10.10.62' (greenmetric's router address) and 127.0.0 (localhost addres) to array
		$whitelist = array(
			'ui.ac.id',
			'mefeedia',
			'ASUS-A455LB'
		);
		
		$ipaddress = gethostbyname(trim(`hostname`));
		$ispname = gethostbyaddr($ipaddress);
		
		foreach ($whitelist as $value) 
			if (strpos($ispname, $value) !== false) 
				return true;
	}