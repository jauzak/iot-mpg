
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	if ( ! function_exists('status_message')){
	    function status_message($status = '', $message = ''){
			die(json_encode(array('status' => $status, 'message' => $message)));
	    }
	}